#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Sep 28 11:37:11 2020

@author: ffespechen
"""

import pandas as pd
import numpy as np

serie1 = pd.Series([1, 3, 5, np.NaN, 11],
                   index=['i0', 'i1', 'i2', 'i3', 'i4'])

print(serie1)

serie1['i3'] = None

print(serie1)

# Filtrar valores NaN
print(serie1.dropna())
print(serie1[serie1.notnull()])
print(serie1[serie1.notna()])

# Reemplazar los valores NaN con otro valor
print(serie1.fillna(0))

df1 = pd.DataFrame([[np.NaN, 2, np.NaN],
                    [0, 3, -1],
                    [np.NaN, np.NaN, np.NaN]], 
                    index=['f1', 'f2', 'f3'],
                    columns=['c1', 'c2', 'c3'])

print(df1)

# Eliminar filas con todos sus valores NaN
print(df1.dropna(how='all'))

# Reemplazar los valores NaN por otros
print(df1.fillna({'c1':-10, 'c2':-20, 'c3':-30}))
