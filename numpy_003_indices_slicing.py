#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Sep 24 23:29:56 2020

@author: ffespechen
"""

import numpy as np

A = np.arange(0,9).reshape(3,3)

print(A)
print('A[1][1] : ', A[1][1])
print('A[1, 1] : ', A[1, 1])

# Seleccionar todos los elementos de una fila
print('A[1, :] -> ', A[1, :])

# Seleccionar todos los elementos de una columna
print('A[:, 2] -> ', A[:, 2])

# Seleccionar elementos de filas y/o columnas no contiguas
print('A[ [0, 0, 2, 2], [0, 2, 0, 2]] -> ', A[[0, 0, 2, 2], [0, 2, 0, 2]])

# Iterando a una ndarray
for filas in A:
    print(filas)

# Usando A.flat
for i in A.flat:
    print(i)

# Aplicando funciones e iteraciones
print(np.apply_along_axis(np.sum, axis=0, arr=A)) # por filas
print(np.apply_along_axis(np.sum, axis=1, arr=A)) # por columnas

    
    
