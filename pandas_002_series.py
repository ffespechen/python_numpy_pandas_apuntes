import pandas as pd 
import numpy as np

# Crear una serie
a = pd.Series([5, 10, 15, 20, 25])
print(a)

# Crear una serie e indicar sus índices
b = pd.Series([3, 6, 9, 12, 15, 18], index=['a', 'b', 'c', 'd', 'e', 'f'])
print(b)

#Acceder a los valores e índices
print(' b.values : ', b.values)
print(' b.values.dtype : ', b.values.dtype)
print(' b.index : ', b.index)

# Selección de elementos de la serie
# Usando el índice numérico
print(a[2])
# Usando el índice alfabético [label] asignado
print(b['f'])
# Un rango de valores
print(a[1:3])
# Los elementos según el índice de la lista pasada como parámetro
print(b[['a', 'c', 'd']])

# Se pueden crear pd.Series a partir de otras estructuras de datos,
# por ejemplo, np.array()
# Tener presente que los valores se pasan por REFERENCIA
# es decir, que si se cambia un valor en el objeto original,
# esta modificación se refleja en la Serie

ndarr = np.array([0, 2, 4, 6, 8])
s1 = pd.Series(ndarr)

print('ndarr : ', ndarr)
print(' s1 : \n',  s1, sep='')

ndarr[2] = 0 

print('Después de la modificación ndarr[2] = 0')
print('ndarr : ', ndarr)
print(' s1 : \n', s1, sep='')

# Filtrar valores: indicar las condiciones entre corchetes
print(s1[s1<7])

# Aplicar funciones u operaciones matemáticas sobre las series
print(np.sin(b))
print(a**3)

# Evaluar los valores de una serie
s2 = pd.Series([10, 5, 10, 7, 5, 0], index=['A', 'B', 'B', 'C', 'A', 'D'])
print(s2)

# Obtener los valores únicos de la Serie, evitando los duplicados
print(s2.unique())

# Mostrar los valores únicos y sus ocurrencias dentro de la Serie
print(s2.value_counts())

# Evaluar si cierto/s valor/es están incluidos en la serie
print(s2.isin([2]))
print(s2.isin([1, 5, 10]))
print(s2[s2.isin([7])])

# valores NaN = not a number
s3 = pd.Series([0.5, 0.7, 0.9, np.NaN, 0.11, 0.13])

print(s3.isnull())
print(s3.notnull())

print(s3[s3.isnull()])
print(s3[s3.notnull()])

# Operaciones con Series
d4 = {'A': 10, 'B': 20, 'C': 30, 'D': 40}
d5 = {'A': 10, 'B': 20, 'F': 50, 'D': 60}

s4 = pd.Series(d4)
s5 = pd.Series(d5)

# Nota: el objeto devuelto solo contiene valores para aquellos labels
# coincidentes en ambas series. Al resto se le asigna NaN
print(s4*s5)

