# Un dataframe es una estructura tabular de datos similar a una hoja de cálculo

import pandas as pd
import numpy as np

# Crear un DataFrame a partir de un diccionario
# Las keys del diccionario se convertirán en los nombres de columnas
datos = {'equipos': ['ROJO', 'AZUL', 'VERDE', 'AMARILLO'], 'puntaje': [100, 200, 50, 75], 'promedio': [1.8, 2.0, 1.2, 0.3]}

competidores = pd.DataFrame(datos)

print(competidores)

# Extraer columnas de un DataFrame
equipos = competidores['equipos']
print(equipos)

# Al momento de crear un DataFrame, podemos elegir qué columnas incorporar
# con la opción index, indicamos los labels para las filas
df2 = pd.DataFrame(datos, columns=['equipos', 'promedio'], index=['uno', 'dos', 'tres', 'cuatro'])
print(df2)

# Crear un DataFrame a partir de ndarrays y arrays
df3 = pd.DataFrame(np.random.random(16).reshape(4,4),
	index=['f1', 'f2', 'f3', 'f4'],
	columns=['c1', 'c2', 'c3', 'c4'])

print(df3)

# Mostrar las columnas, índices y valores del DataFrame
print('columnas : ', df3.columns)
print('Filas : ', df3.index)
print('Valores :')
print(df3.values)

# Acceder a las columnas de un DataFrame
print(df3['c3']) # usando notación de array/índice
print(df3.c2)    # usando notación de punto

# Acceder a las filas del DataFrame
# Indice alfanumericos
print(df3.loc['f1'])
print(df3.loc[['f2', 'f4']])
# Índices numéricos
print(competidores.loc[[0, 2]])
print(df3[0:3])

# Acceder a un elemento en particular
print(df3['c4']['f4'])

# Hacer el DataFrame más autodescriptivo
df3.index.name = 'FILAS'
df3.columns.name = 'COLUMNAS'
print(df3)

# Agregar una columna nueva al DataFrame
# Con un único valor para todas las filas
df3['nuevo_unico'] = 0.0
print(df3)
# Con un valor específico para cada fila
df3['nuevo_diferente'] = [0.1, 0.2, 0.3, 0.4]
print(df3)

# Modificar los valores de una columna completa
valor_nuevo = np.arange(4)
df3['nuevo_diferente'] = valor_nuevo
print(df3)

competidores.index = [3, 2, 0, 1]
competidores['ranking'] = valor_nuevo
print(competidores)

# Si se usa una pd.Serie identifica los índices para la correspondencia de valores
valor_nuevo_serie = pd.Series(np.random.random(3))
competidores.index = [2, 1, 3, 0]
competidores['valor_random'] = valor_nuevo_serie
print(competidores)

# Identificar si un elemento está en el DataFrame
# se utiliza una el método isin() como en las pd.Series
print(df3.isin([1, 3, 0.0]))

# Puede utilizarse como filtro de visuzalización
print(df3[df3.isin([1,3, 0.0])])

# Eliminar una columna (usando del)
del df3['nuevo_diferente']
print(df3)

# Filtrar datos
print(competidores[competidores.puntaje < 120])

# Si se utilizan diccionarios anidados para crear los DataFrame:
# - Las keys externas se convierten en nombres de columnas
# - Las keys internas se convierten en nombres de filas
# - Si faltan coincidencias, se reemplazan por NaN

diccionario_anidado = {'EQUIPO_1': {'2019': 100, '2018': 200, '2017': 150}, 
					'EQUIPO_2': {'2020': 0, '2019': 120, '2016': 500},
					'EQUIPO_3': {'2019': 320, '2017': 155, '2016': 320}}
df4 = pd.DataFrame(diccionario_anidado)
print(df4)

# Transposición
print(df4.T)
