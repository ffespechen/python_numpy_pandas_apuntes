#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Sep 27 16:36:54 2020

@author: ffespechen
"""

import numpy as np
import matplotlib.pyplot as plt

r = np.linspace(0, 5, 360)
angulos = np.linspace(0, 6*2*np.pi, 360)

x = np.cos(angulos)*r
y = np.sin(angulos)*r

plt.plot(x, y)
plt.axis([-5, 5, -5, 5])
plt.show()