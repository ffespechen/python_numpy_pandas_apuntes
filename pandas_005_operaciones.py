#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Sep 27 20:30:26 2020

@author: ffespechen
"""
import pandas as pd
import numpy as np

# Están definidos los siguientes métodos 'flexibles' para las operaciones 
# matemáticas fundamentales
# add()
# sub()
# mul()
# div()

df1 = pd.DataFrame(np.arange(16).reshape(4,4),
                   index=['f1', 'f2', 'f3', 'f4'],
                   columns=['c1', 'c2', 'c3', 'c4'])

df2 = pd.DataFrame(np.random.randint(0,10,12).reshape(3,4),
                   index=['f1', 'fx', 'f3'],
                   columns=['c1', 'c2', 'c3', 'cx'])

print(df1)
print(df2)

print(df1.add(df2))
print(df1.mul(df2))

print(np.sqrt(df2))

# Operando con una pd.Serie
# Las operaciones se aplican columna a columna
s1 = pd.Series([0,1,0,-1,2], index=['c1', 'c2', 'c3', 'c4', 'cx'])
print(s1)

# Las columnas no coincidentes se completan con NaN
print(df1.add(s1))
print(df2.add(s1))

# Definir una función por fila o columna utilizando apply()

f = lambda x : (x - x.mean())**2

# DataFrame
print('DF2')
print(df2)

# Por columnas
print('COLUMNAS')
print(df2.apply(f, axis=0))

# Por filas
print('FILAS')
print(df2.apply(f, axis=1))


# Caso práctico. Una función que devuelve los máximos, mínimos y promedios 
def func(x):
    return pd.Series([x.min(), x.max(), x.mean()], index=['min', 'max', 'prom'])

# por columns axis=0
print('COLUMNAS')
print(df2.apply(func, axis=0))

# por filas axis=1
print('FILAS')
print(df2.apply(func, axis=1))


# Estadísticas
print(df2.sum(axis=0))
print(df2.sum(axis=1))
print(df2.describe())

# Ordenar los índices
df3 = pd.DataFrame(np.random.randint(0, 10, 15).reshape(3,5),
                   index=['f1', 'f3', 'f2'],
                   columns=['c3', 'c4', 'c2', 'c1', 'c5'])

print('DF3')
print(df3)

# Por filas
print(df3.sort_index('index'))
# Por columnas
print(df3.sort_index('columns'))

# Ordenar los datos
print(df3.sort_values(by=['c1', 'c3'], ascending=True))


