#!/usr/bin/python3

import numpy as np

# np.load()
# np.save()

datos = np.random.random((30,20))
ruta = 'datos/datos_20200926'
ruta_tabulados = 'datos/datos_tab.csv'

np.save(ruta, datos)

datos_leidos = np.load(ruta+'.npy')

print(datos_leidos/datos)

# leer datos tabulados en archivos
# nombre del archivo
# delimiter = str
# names = True/False -> títulos
datos_tabulados = np.genfromtxt(ruta_tabulados, delimiter=';', names=True)
print(datos_tabulados)

