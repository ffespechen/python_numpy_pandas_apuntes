#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Sep 25 16:19:55 2020

@author: ffespechen
"""

import numpy as np

A = np.random.random((4,4))

print(A)
print('A < 0.5 ')
B = A<0.5
print(B)
print('A[ A < 0.5]')
print(A[A<0.5])
print('A * B')
print(A*B)

# Manipulación de la shape de un ndarray

a = np.random.random((3,4))

print('a')
print(a)

# reshape() devuelve un objeto con la nueva forma

A = a.reshape((4,3))
print('A = a.reshape((d1, d2))')
print(A)

# shape = () cambia al objeto mismo
a.shape = (6,2)

print('a.shape = (d1, d2)')
print(a)

# Para convertir un objeto n-dimensional en uno uni-dimensional
# se puede usar ravel()

C = a.ravel()

print(' a.ravel() ')
print(C)

# Para obtener la transpuesta, se usa transpose()
print(' a.transpose() ')
print(a.transpose())
