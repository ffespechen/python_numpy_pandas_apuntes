#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Sep 24 22:33:45 2020

@author: ffespechen
"""

# Operaciones aritméticas con ndarray

import numpy as np

a = np.arange(0,10)

print('ndarray a : ', a)
print(' a - 1 : ', a-1)
print(' a / 5 : ', a/5)

# Operaciones element-wise

b = np.arange(-10, 0, 1)

print('ndarray b : ', b)
print(' a + b : ', a+b)
print(' a * b : ', a*b)


# Aplicando funciones elemento-a-elemento en un ndarray
e = np.arange(0, 180, 15)
print(np.sin(np.radians(e)))

# Operaciones con ndarray multidimensionales
F = np.arange(90,99).reshape((3, 3))
G = np.zeros((3,3))
print(F)
print(G)
print(' F * G : ')
print(F*G)

# Producto Matricial

H = np.arange(0,9).reshape((3,3))
print(' np.dot(F, H) :\n', np.dot(F, H))

I = np.array([[1, 0, 0],
              [0, 1, 0],
              [0, 0, 1]])
print(F.dot(I))

# Funciones de agregación
# Reciben un dnarray como parámetro y devuelven un único valor

J = np.random.random(5)
print('J : ', J)
print('np.min(J) : ', np.min(J))
print('np.max(J) : ', np.max(J))
print('np.mean(J) : ', np.mean(J))
print('np.std(J) : ', np.std(J))

