#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Sep 27 16:26:48 2020

@author: ffespechen
"""

import pandas as pd
import numpy as np

serie1 = pd.Series([1, 3, 5, 7, 11], index=['A', 'B', 'C', 'D', 'E'])
print(serie1)

# Obtener el mínimo y máximo valor de los índices
print(serie1.idxmin())
print(serie1.idxmax())

# Saber si los índices son únicos (es decir, no hay valores repetidos)
print(serie1.index.is_unique)

# Reindexing
# La función reindex() devuelve una nueva Serie de datos, ordenados según la 
# nueva secuencia. Se pueden eliminar indices o crear nuevos, en cuyo caso
# se agrega NaN a los valores no disponibles
s2 = pd.Series([1, 2, 3, 4, 5],index=['uno', 'dos', 'tres', 'cuatro', 'cinco'])
print(s2)
s3 = s2.reindex(['cuatro', 'seis', 'cero', 'dos', 'tres'])
print(s3)

# Interpolar o agregar valores al momento de reindexar
s4 = pd.Series([1,2,3,4,5], index=[0,3,6,8,10])
print(s4)

# parámetro method='ffill'
print(s4.reindex(range(10), method='ffill'))

# parámetro method='bfill'
print(s4.reindex(range(10), method='bfill'))


# Dropping
# Borra (delete) las filas indicadas y devuelve un nuevo objeto sin ellas
print(s2.drop(['cuatro', 'dos']))

# Para borrar filas o columnas en un DataFrame, hay que indicar el eje
df3 = pd.DataFrame(np.random.random(16).reshape(4,4),
	index=['f1', 'f2', 'f3', 'f4'],
	columns=['c1', 'c2', 'c3', 'c4'])

print(df3)

print(df3.drop(['c2', 'c4'], axis=1))
print(df3.drop(['f1', 'f3'], axis=0))

# Alignment