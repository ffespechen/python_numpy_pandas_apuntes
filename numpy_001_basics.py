#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Sep 24 21:49:51 2020

@author: ffespechen
"""

# Numpy basics
# ndarray (por N-dimensional array) 
#   es un array multidimensional, homogeneo con una cantidad predeterminada
#   de ítems, definida en el momento de su creación
# dtype (por data-type) es un objeto de NumPy que define el tipo de dato
# shape es una tupla con la cantidad elementos por dimensión de un ndarray
# axes son las dimensiones de un ndarray
# rank es la cantidad de axes de un ndarray

import numpy as np

# Crear un ndarray
a = np.array([1, 3, 5, 8])

print('ndarray a : ', a)
print('Tipo de a : ', type(a))
print('Dimensiones de a : ', a.ndim)
print('Shape de a : ', a.shape)
print('dtype de los elementos de a :', a.dtype)

b = np.array([[1.1, 2.2, 3.3, 4.4], [0.0, 0.0, 0.0, 0.0]])

print('ndarray b : ', b)
print('Tipo de b : ', type(b))
print('Dimensiones de b : ', b.ndim)
print('Shape de b : ', b.shape)
print('dtype de los elementos de b : ', b.dtype)

# Definición explícita del tipo de dato contenidos

c = np.array([[1, 2, 3], [4, 5, 6]], dtype='U1')
print(c)

# Crear ndarrays con valores y tamaño predeterminados 
# La tupla que se pasa como parámetro indica filas * columnas
d = np.zeros((4,3))
print(d)
e = np.ones((8,2), dtype='int32')
print(e)

# Creación de secuencias de valores
f = np.arange(0, 10)
print(f)

g = np.arange(100, 0, -25)
print(g)

# Redefinir la figura (shape) de un ndarray
h = np.arange(0,12).reshape(4,3)
print(h)

# Definir un rango de valores con determinada cantidad de elementos
# linspace(a, b, c) 
# donde a = inicial, b = final (se incluye), c = partes en que debe dividirse
i = np.linspace(1, 10, 30).reshape(15, 2)
print(i)

# Crear un array con valores aleatorios
j = np.random.random((3,5))
print(j)

