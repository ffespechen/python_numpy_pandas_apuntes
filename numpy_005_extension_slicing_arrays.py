import numpy as np 

# Para unir (joining) ndarrays se pueden utilizar las funciones
# hstack()
# vstack()

A = np.zeros((3,3))
B = np.ones((3,3))


print('A ', A)
print('B ', B)

# Horizontal
print('hstack(A, B)')
print(np.hstack((A, B)))

# Vertical
print('vstack(A, B)')
print(np.vstack((A, B)))

# Combinación
print('hstack((vstack((A,B)), vstack((B,A))))')
print(np.hstack((np.vstack((A,B)), np.vstack((B,A)))))

# Para combinar vectores, se puede utilizar 
# column_stack()
# row_stack()

X = np.array([1, 1, 1, 1])
Y = np.array([2, 2, 2, 2])
Z = np.array([3, 3, 3, 3])

print('X ', X)
print('Y ', Y)
print('Z ', Z)

print('column_stack')
print(np.column_stack((X, Y, Z)))

print('row_stack')
print(np.row_stack((X, Y, Z)))

# Obtener porciones (slicing) de los ndarray
# hsplit()
# vsplit()
# split()

M = np.arange(16).reshape((4,4))

print(M)
print(np.hsplit(M, 2))

print(np.vsplit(M, 2))

# axis=0 -> filas
# axis=1 -> columnas

print('Por columnas axis=1')
print(np.split(M, [1,2], axis=1))

print('Por filas axis=0')
print(np.split(M, [1,2,3], axis=0))

