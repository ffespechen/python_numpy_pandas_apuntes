#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Oct  1 18:25:46 2020

@author: ffespechen
"""

# pandas I/O API Tools - CSV readers and writers

import pandas as pd
import numpy as np

# Separador por defecto = ,
datos_a = pd.read_csv('datos/ch05_a.csv')
print(datos_a)

# Separador = ';'
datos_b = pd.read_csv('datos/ch05_b.csv', sep=';')
print(datos_b)

# Sin encabezado
datos_c = pd.read_csv('datos/ch05_c.csv', header=None, sep=';')
print(datos_c)

# Sin encabezado, con parámetro names para las columnas
datos_d = pd.read_csv('datos/ch05_d.csv', names=['col0', 'col1', 'col2', 'col3', 'col4', 'animal'])
print(datos_d)


#--- Uso de chunksize
salida = pd.Series(object)
medias = pd.Series(object)
i = 0

# chunksize = n donde n es la cantidad de filas en las cuales hará el "corte"
piezas = pd.read_csv('datos/ch05_1.csv', chunksize=2)

for pieza in piezas:
    print(pieza)
    salida[i] =  pieza['white'].sum()
    medias[i] = pieza['green'].mean()
    i += 1
    
print(salida)
print(medias)


# Escribir en archivos CSV
df1 = pd.DataFrame( np.random.random(120).reshape(24, 5), 
                   index=np.random.choice(list('ABCDEFGHIJKLMNOPQRSTUVWXYZ'), 
                                          size=24,
                                          replace=False),
                   columns=['dat1', 'dat2', 'dat3', 'dat4', 'dat5'])

df1.to_csv('datos/ch05_rand.csv')

df1.to_csv('datos/ch05_rand1.csv', index=False, header=False)

df2 = pd.read_csv('datos/ch05_rand.csv', index_col=0)

print('********* DATOS LEÍDOS *********')
print(df2)

print('********* PROMEDIO COLUMNAS *********')
print(df2.mean(axis=0))

print('********* PROMEDIO FILAS *********')
print(df2.mean(axis=1))
    