#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Oct  2 20:29:51 2020

@author: ffespechen
"""

import pandas as pd
import numpy as np

df1 = pd.DataFrame(np.random.random((10,4)),
                   index=np.arange(10),
                   columns=['dat1', 'dat2', 'dat3', 'dat4'])

print(df1.to_html())

s = ['<html>']
s.append('<head><title>DATA FRAME TO HTML</title></head>')
s.append('<body>')
s.append(df1.to_html())
s.append('</body>')
s.append('</html>')

contenido = ''.join(s)

f2 = open('datos/df_to_html.html', 'w')
f2.write(contenido)
f2.close()

print(df1.to_latex())

print(df1.to_json())


